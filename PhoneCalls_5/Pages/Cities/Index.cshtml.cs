using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PhoneCalls_5.Models;

namespace PhoneCalls_5.Pages.Cities
{
    public class IndexModel : PageModel
    {
        private readonly PhoneCalls_5.Models.PhoneCallsContext _context;

        public IndexModel(PhoneCalls_5.Models.PhoneCallsContext context)
        {
            _context = context;
        }

        public IList<City> City { get;set; }

        public async Task OnGetAsync()
        {
            City = await _context.Cities.ToListAsync();
        }
    }
}
