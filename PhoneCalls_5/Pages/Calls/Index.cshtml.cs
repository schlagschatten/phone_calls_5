using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PhoneCalls_5.Models;

namespace PhoneCalls_5.Pages.Calls
{
    public class IndexModel : PageModel
    {
        private readonly PhoneCalls_5.Models.PhoneCallsContext _context;

        public IndexModel(PhoneCalls_5.Models.PhoneCallsContext context)
        {
            _context = context;
        }

        public IList<PhoneCalls_5.Models.Calls> Calls { get;set; }

        public async Task OnGetAsync()
        {
            Calls = await _context.Calls.ToListAsync();
        }
    }
}
