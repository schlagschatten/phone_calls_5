using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PhoneCalls_5.Models;

namespace PhoneCalls_5.Pages.Calls
{
    public class DetailsModel : PageModel
    {
        private readonly PhoneCalls_5.Models.PhoneCallsContext _context;

        public DetailsModel(PhoneCalls_5.Models.PhoneCallsContext context)
        {
            _context = context;
        }

        public PhoneCalls_5.Models.Calls Calls { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Calls = await _context.Calls.FirstOrDefaultAsync(m => m.id == id);

            if (Calls == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
