using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PhoneCalls_5.Models;

namespace PhoneCalls_5.Pages.Calls
{
    public class EditModel : PageModel
    {
        private readonly PhoneCalls_5.Models.PhoneCallsContext _context;

        public EditModel(PhoneCalls_5.Models.PhoneCallsContext context)
        {
            _context = context;
        }

        [BindProperty]
        public PhoneCalls_5.Models.Calls Calls { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Calls = await _context.Calls.FirstOrDefaultAsync(m => m.id == id);

            if (Calls == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Calls).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CallsExists(Calls.id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool CallsExists(int id)
        {
            return _context.Calls.Any(e => e.id == id);
        }
    }
}
