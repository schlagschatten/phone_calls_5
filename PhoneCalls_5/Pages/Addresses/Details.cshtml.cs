using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PhoneCalls_5.Models;

namespace PhoneCalls_5.Pages.Addresses
{
    public class DetailsModel : PageModel
    {
        private readonly PhoneCalls_5.Models.PhoneCallsContext _context;

        public DetailsModel(PhoneCalls_5.Models.PhoneCallsContext context)
        {
            _context = context;
        }

        public Address Address { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Address = await _context.Addresses.FirstOrDefaultAsync(m => m.id == id);

            if (Address == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
